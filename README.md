# Test Automation with Behavior Driven Development #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The repository contains the test automation project of Twin One

### Test Automation Practices###

* Test scenarios is advised to be written independently. A result of a scenario should not affect another scenario but to itself, these effects should be minimized. 
* Test scenarios is tried to be written as short and understandable and ideally it should include 1 Given 1 When 1 Then step.



Scenario: Login from the signup page 

		Given I navigate to "https://dev.twin-one.io/signin" page 
		When I login with username "idil.agabeyoglu@gmail.com" and password "testUser123*" 
		Then I should be navigated to "Properties List" page 	

* Cucumber tool is used to reach out a variety of stakeholders since its language Gherkin allows the test automation to be understandable by not only the QA yet also business and developers. 

* In Cucumber, Background(that runs before Scenario), Given (precondition), When(action), Then(expected result) tags are used. Although Gherkin allows to use And tag, it is not suggested to use it since it might create a hard-to-understand and complicated sentences.

-In ideal scenario, common steps of the feature are added as Given under the Background section such as setting up Browser, adding data, and login. By doing so, it is possible to increase the number of preconditions while decreasing them, Given, When and Then tags, in test scenarios.  

-In cases where commonality cannot be applied can be illustrated as logging in with different passwords and email combinations. 
 
* @FeatureName tag is added before the description of scenarios. 

* While automatizing test cases, Test Engineer can determine the cover of scope on himself according to the conducted risk analyses and the returned outcomes. On the other hand, in case an error has been received from the cases that are not covered. Hence, it is advised to cover that case in test automation, too. 

* Page Object Model(POM) is used to decrease the repetition of the code, ease the maintenance and readability of the code. 

* It is advised to use; 

	-Intelij IDEA as IDE,  

	-Language as Java 

	-Selenium Webdriver for web tests  
	

### How do I get set up? ###

* Step 1: Install Java JDK 8
Although there are newer versions of Java, Java 8 is the most steady one to work with other tools, libraries etc. Hence, the usage of Java 8 is adviced to be used for the 
sake of stability.

https://www.oracle.com/java/technologies/downloads/#java8-mac


* Step 2: Install Maven

a. Download Homebrew from command line with the below given command:
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

b. Download Maven from command line with the below given command:
brew install maven


* Step 3: Set JAVA_HOME as environment variable

-In order to set JAVA_HOME, the below given path are added to the bash profile (Additionally, bash_profile can be open with: "open ~/.bash_profile" command)

open ~/.bash_profile

export JAVA_HOME=****** (Location of Java path until bin file)
export PATH=$JAVA_HOME/bin:$PATH

-In order to validate the JAVA HOME is set up correctly the below given code is used.

echo $JAVA_HOME


* Step 4: Set MAVEN_HOME as environment variable

-In order to set MAVEN_HOME, the below given path are added to the bash profile (Additionally, bash_profile can be open with: "open ~/.bash_profile" command)

open ~/.bash_profile

export MAVEN_HOME=****** (Location of Maven path until bin file)
export PATH=$MAVEN_HOME/bin:$PATH


-In order to validate the MAVEN_HOME is set up correctly the below given code is used.

echo $MAVEN_HOME


P.S: Sample bash_profile is given under below. 

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_202.jdk/Contents/Home
export MAVEN_HOME=/opt/homebrew/Cellar/maven/3.8.3/libexec

export PATH=$MAVEN_HOME/bin:$PATH
export PATH=$JAVA_HOME/bin:$PATH


* Step 5: Install Intellij

https://www.jetbrains.com/idea/download/#section=mac


* Step 6: Add additional plugins

a. Go to Intellij Preferences/Plugins

b. Add the following plugins
	Cucumber for Java
	Gherkin
	JUnit


### How to run tests? ###

Type the command "mvn test" from terminal


### Who do I talk to? ###

* Team to be contacted